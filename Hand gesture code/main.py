import cv2
import mediapipe as mp
import numpy as np
import socket
import time

mp_hands = mp.solutions.hands
hands = mp_hands.Hands()

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()

    frame = cv2.flip(frame, 1)
    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    results = hands.process(rgb_frame)

    hand_data = None

    if results.multi_hand_landmarks:
        for hand_landmarks in results.multi_hand_landmarks:
            # Draw hand landmarks
            mp.solutions.drawing_utils.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)

            # Get 2D coordinates for the tip and base of the thumb and index finger
            thumb_tip = np.array([hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_TIP].x,
                                  hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_TIP].y])
            thumb_base = np.array([hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_MCP].x,
                                   hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_MCP].y])
            index_tip = np.array([hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].x,
                                  hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].y])
            index_base = np.array([hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_MCP].x,
                                   hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_MCP].y])

            thumb_orientation = "Up" if thumb_tip[1] < index_base[1] else "Down"

            finger_orientation = "Right" if index_tip[0] > thumb_base[0] else "Left"

            hand_data = f"{thumb_orientation},{finger_orientation}"

            closed_threshold = 0.15 
            is_thumb_closed = (
                np.linalg.norm(thumb_tip - index_base) < closed_threshold
            )
            is_finger_closed = (
                np.linalg.norm(index_tip - thumb_base) < closed_threshold
            )

            # Display orientations based on hand position
            if is_thumb_closed and is_finger_closed:
        
                cv2.putText(frame, f"Thumb and Finger: Closed", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2,
                            cv2.LINE_AA)
            elif is_thumb_closed:
                cv2.putText(frame, f"Thumb: Closed", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2,
                            cv2.LINE_AA)
                cv2.putText(frame, f"Finger: {finger_orientation}", (10, 70), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2,
                            cv2.LINE_AA)
            elif is_finger_closed:
                cv2.putText(frame, f"Thumb: {thumb_orientation}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2,
                            cv2.LINE_AA)
                cv2.putText(frame, f"Finger: Closed", (10, 70), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2,
                            cv2.LINE_AA)
            else:
                cv2.putText(frame, f"Thumb: {thumb_orientation}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2,
                            cv2.LINE_AA)
                cv2.putText(frame, f"Finger: {finger_orientation}", (10, 70), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2,
                            cv2.LINE_AA)
            

    cv2.imshow('Hand Gesture Recognition', frame)

    if hand_data:
        with open('hand_data.txt', 'a') as file:
            file.write(hand_data + '\n')

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()