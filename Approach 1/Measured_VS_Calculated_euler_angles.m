clc; clear all; close all;
load("output_pinky.mat")
t = ans(1,:);
pitch_meas = ans(2,:);
pitch_in = ans(4,:);

hfig1 = figure(1);
plot(t, pitch_in, 'Color', '[0.8500 0.3250 0.0980]', 'Linestyle', '--', 'LineWidth', .2)
hold on
plot(t, pitch_meas, 'Color', '[0 0.4470 0.7410]', 'Linestyle', '-', 'LineWidth', 1.5)

applyCustomPlotSettings(hfig1, 'Time $(s)$', 'Angle $\theta_y$ [Rad]', {'Pitch input', 'Pitch measured'})

hfig2 = figure(2);
elevation_meas = ans(5,:);
elevation_in = ans(7,:);
plot(t,elevation_in, 'Color', '[0.8500 0.3250 0.0980]', 'Linestyle', '--', 'LineWidth', 2)
hold on
plot(t,elevation_meas, 'Color', '[0 0.4470 0.7410]', 'Linestyle', '-', 'LineWidth', 2)
applyCustomPlotSettings(hfig2, 'Time $(s)$', 'Angle $\theta_z$ [Rad]',  {'Elevation input', 'Elevation measured'})