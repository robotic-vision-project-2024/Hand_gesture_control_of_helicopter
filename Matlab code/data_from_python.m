function [pitch,elevation] = data_from_python(array)

    pitch = zeros(2,length(array));
    elevation = zeros(2,length(array));
    
    for i = 1:length(array)
        if array(1,i) == "Up"
            elevation(1,i) = 1;
        else
            elevation(1,i) = -1;
        end
        
        if array(2,i) == "Left"
            pitch(2,i) = 1;
        else
            pitch(2,i) = -1;
        end
    end
        
       
end