close all;
% FOR HELICOPTER NR 1-2
% This file contains the initialization for the helicopter assignment in
% the course TTK4115. Run this file before you execute QuaRC_ -> Build 
% to build the file heli_q8.mdl.

% Oppdatert h�sten 2006 av Jostein Bakkeheim
% Oppdatert h�sten 2008 av Arnfinn Aas Eielsen
% Oppdatert h�sten 2009 av Jonathan Ronen
% Updated fall 2010, Dominik Breu
% Updated fall 2013, Mark Haring
% Updated spring 2015, Mark Haring


%%%%%%%%%%% Calibration of the encoder and the hardware for the specific
%%%%%%%%%%% helicopter
Joystick_gain_x = - 1.5;
Joystick_gain_y = 1.5;


%%%%%%%%%%% Physical constants
g = 9.81; % gravitational constant [m/s^2]
l_c = 0.40; % distance elevation axis to counterweight [m]
l_h = 0.66; % distance elevation axis to helicopter head [m]
l_p = 0.175; % distance pitch axis to motor [m]
m_c = 1.92; % Counterweight mass [kg]
m_p = 0.65; % Motor mass [kg]
v_s = 6.25;
k_f = 6.25;
v_s_0 = 6.25;

k1 = k_f/(2*m_p*l_p);
k2 = k_f*l_h/(m_c*l_c^(2)+2*m_p*l_h^(2));



%TESTS
%test 1 Should be marginally stable, but is not
%lambda1 = -3i;
%lambda2 = 3i;
%test 2 
%lambda1 =  0.07;
%lambda2 =  0.07;
%test 3 
lambda1 = -1 - 2i;
lambda2 = -1 + 2i;
%test 4 
%lambda1 = 0.05 - 2i;
%lambda2 = 0.05 + 2i;
%test 5 
%lambda1 = -2i;
%lambda2 = 2i;


A = [0 1 0 0 0;0 0 0 0 0; 0 0 0 0 0; -1 0 0 0 0; 0 0 -1 0 0];
B = [0 0; 0 k1; k2 0; 0 0; 0 0];

k_pd = -lambda1 -lambda2;
k_pp  = lambda1*lambda2;


%% Test
n = 20;
string1 = 'Up';
string2 = 'Down';
string3 = 'Left';
string4 = 'Right';

array_size = [1,n];

random_int_1 = randi([1,2], array_size);

random_array_1 = cell(array_size);
random_array_1(random_int_1 == 1) = {string1};
random_array_1(random_int_1 == 2) = {string2};

random_int_2 = randi([1,2], array_size);

random_array_2 = cell(array_size);
random_array_2(random_int_2 == 1) = {string3};
random_array_2(random_int_2 == 2) = {string4};

random_array = [random_array_1;random_array_2];




    pitch = zeros(1,length(random_array));
    elevation = zeros(1,length(random_array));
    
    for i = 1:length(random_array)
        if strcmp(random_array(1,i), 'Up')
            elevation(1,i) = 1;
        else
            elevation(1,i) = -1;
        end
        
        if strcmp(random_array(2,i),'Left')
            pitch(1,i) = 1;
        else
            pitch(1,i) = -1;
        end
    end
    ts = zeros(2,length(total_kaos));
    
    
    time_steps = (1:1:20)';
    total_kaos = [elevation; pitch];
    total_kaos_ts = timeseries(total_kaos', time_steps);


