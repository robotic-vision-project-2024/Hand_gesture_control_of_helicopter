function handData = getHandData(filePath)
    % Read all lines from the file
    fid = fopen(filePath, 'r');
    handDataCell = textscan(fid, '%s', 'Delimiter', '\n');
    fclose(fid);

    % Extract orientation from each line
    orientations = handDataCell{1};
    
    thumb_orientations = cell(1, numel(orientations));
    finger_orientations = cell(1, numel(orientations));
    
    for i = 1:numel(orientations)
        parts = strsplit(orientations{i}, ',');
        thumb_orientations{i} = parts{1};
        finger_orientations{i} = parts{2};
    end
    
    % Convert thumb and finger arrays to a 2xN cell array
    handData = [thumb_orientations; finger_orientations];
end